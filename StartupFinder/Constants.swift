//
//  Constants.swift
//  StartupFinder
//
//  Created by Hassaan on 08/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import Foundation


struct Constants {
    internal static let profilePictureLocalId = "profilePicture.localIdentifier";
    
    internal static let ZLSwipeableViewBackgroundColorViewTag : Int = 12;
    internal static let ZLSwipeableViewLikeDislikeLabelTag : Int = 11;
    
    
    // Server 
    internal static let kBaseURL : String = "http://Localhost:3000/";
    internal static let kUsers = "users";
    
    internal struct Notifications {
        internal static let IsSignIn: String = "constants.notifications.signIn";
    }

    
}

