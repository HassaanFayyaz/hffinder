//
//  Constant.h
//  StartupFinder
//
//  Created by Hassaan on 25/06/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

// MARK: Settings


#define kMaximumTeamSize            "startup.maximumTeamSize"
#define kMaximumTeamSizeDefault     25
#define kStartupType                "startup.startupType"
#define kStartupTypeDefault         "Joint Venture"


#define safeSet(d,k,v) if (v) d[k] = v;


#endif /* Constant_h */
