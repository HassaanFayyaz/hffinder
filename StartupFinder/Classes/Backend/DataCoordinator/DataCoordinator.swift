//
//  DataCoordinator.swift
//  StartupFinder
//
//  Created by Hassaan on 16/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit

class DataCoordinator: NSObject {

    /* private */ var currentUSer : User! ;
    
    class var shared: DataCoordinator {
        struct Static {
            static var onceToken: dispatch_once_t = 0;
            static var instance: DataCoordinator? = nil;
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = DataCoordinator();
        }
        return Static.instance!;
    }
    
//    func shared() -> DataCoordinator {
//        return shared;
//    }
//    
    
    func getCurrentUser() -> User {
        if (self.currentUSer) == nil {
            self.currentUSer = User.MR_createEntity();
        }
        return self.currentUSer;
    }
}
