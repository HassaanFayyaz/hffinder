//
//  LogInManager.swift
//  StartupFinder
//
//  Created by Hassaan on 14/08/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit

class LogInManager: NSObject {
    
    static let shared = LogInManager()
    
    var isSignedIn = false
    var displayName: String?
    var photoUrl: NSURL?
}
