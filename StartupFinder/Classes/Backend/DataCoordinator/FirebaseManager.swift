//
//  FirebaseManager.swift
//  StartupFinder
//
//  Created by Hassaan on 25/08/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import Firebase

class FirebaseManager: NSObject {
    
    static let shared = FirebaseManager();

    private var _refHandle: FIRDatabaseHandle!
    var ref: FIRDatabaseReference!
    var users: [FIRDataSnapshot]! = []
    var msglength: NSNumber = 100

    
    
    internal func retrieveUsers () {
        
        ref = FIRDatabase.database().reference()

        // Listen for new messages in the Firebase database
        _refHandle = self.ref.child("Users2").observeEventType(.ChildAdded, withBlock: { (snapshot) -> Void in
//            let text = snapshot.value!["userName"] as? String
//            let sender = snapshot.value!["firstName"] as? String
//            print("First Name : \(text) \nUserName : \(sender)");
            self.users.append(snapshot)
            
            let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;
            appDelegate.page2?.swipeableView.loadViews();
            
        })
    }


}
