//
//  User.swift
//  StartupFinder
//
//  Created by Hassaan on 13/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import Foundation
import CoreData



class User: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    func toDictionary() -> NSDictionary {
        let jsonable: NSMutableDictionary = NSMutableDictionary()
        jsonable.setValue(self.firstName, forKey: "firstName");
        jsonable.setValue(self.middleName, forKey: "middleName");
        jsonable.setValue(self.lastName, forKey: "lastName");
        jsonable.setValue(self.userName, forKey: "userName");
//        jsonable.setValue(self., forKey: "placename");
//        jsonable.setValue("EMPTY", forKey: "categories");
//        jsonable.setValue("EMPTY", forKey: "_id");
        return jsonable
    }
    
}
