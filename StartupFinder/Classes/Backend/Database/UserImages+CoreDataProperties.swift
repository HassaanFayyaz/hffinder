//
//  UserImages+CoreDataProperties.swift
//  StartupFinder
//
//  Created by Hassaan on 13/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserImages {

    @NSManaged var image1: String?
    @NSManaged var image2: String?
    @NSManaged var user: User?

}
