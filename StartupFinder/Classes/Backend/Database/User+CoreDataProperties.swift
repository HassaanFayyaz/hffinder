//
//  User+CoreDataProperties.swift
//  StartupFinder
//
//  Created by Hassaan on 13/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var firstName: String?
    @NSManaged var middleName: String?
    @NSManaged var lastName: String?
    @NSManaged var userName: String?
    @NSManaged var email: String?
    @NSManaged var password: String?
    @NSManaged var city: String?
    @NSManaged var country: String?
    @NSManaged var userImages: NSManagedObject?

}
