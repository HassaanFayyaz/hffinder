//
//  BaseSettingsScreen.swift
//  StartupFinder
//
//  Created by Hassaan on 25/06/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit

class BaseSettingsScreen: UITableViewController {
    
    // Private Data Members
    @IBOutlet weak var lblGetStartupPRO: UILabel!
    @IBOutlet weak var lblStartupTypeValue: UILabel!
    
    @IBOutlet weak var sliderMaxTeamSize: UISlider!
    @IBOutlet weak var lblMaxTeamSizeValue: UILabel!
    
    internal func BaseSettingsScreen() -> UIViewController {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Settings", bundle: nil);
        let me  = storyboard.instantiateViewControllerWithIdentifier("BaseSettingsScreen") ;
        return me;
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sliderValue = NSUserDefaults.standardUserDefaults().integerForKey(kMaximumTeamSize);
        self.sliderMaxTeamSize.value = Float(sliderValue);
        self.lblMaxTeamSizeValue.text = "\(sliderValue)";

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);

        self.lblStartupTypeValue.text = NSUserDefaults.standardUserDefaults().stringForKey(kStartupType);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//     MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true);
        
    }
    

    
//    MARK: - User Interaction
    @IBAction func MaxTeamSizeSliderValueChanged(sender: UISlider) {
        
        let value : Int = Int(sender.value)
        self.lblMaxTeamSizeValue.text =  "\(value)"  ;
    }

    @IBAction func maxTeamSizeSliderChangeEnded(sender: UISlider) {
        
        let value : Int = Int(sender.value);
        NSUserDefaults.standardUserDefaults().setInteger(value, forKey: kMaximumTeamSize);
    }
    @IBAction func btnDonePressed(sender: UIBarButtonItem) {
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: { 
            
        })
    }
}
