//
//  SignInScreen.swift
//  StartupFinder
//
//  Created by Hassaan on 12/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import KRProgressHUD
import Firebase
import FirebaseAuth


class SignInScreen: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

//        let tapGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:"dismissKeyboardOnTap"));
//        self.view.addGestureRecognizer(tapGestureRecognizer);

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    internal func SignInScreen() -> UIViewController {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "SignIn", bundle: nil)
        let me = storyboard.instantiateViewControllerWithIdentifier("SignInScreen")
        return me;
    }

    
    @IBAction func CancelButtonPressed(sender: UIBarButtonItem) {
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil);
        
    }
    
    @IBAction func signInButtonPressed(sender: UIButton) {
        
        KRProgressHUD.show(progressHUDStyle: .BlackColor, message: "Signing in...")
        
        let delay = dispatch_time(DISPATCH_TIME_NOW, Int64(2.0 * Double(NSEC_PER_SEC)))
        dispatch_after(delay, dispatch_get_main_queue()) {
//            KRProgressHUD.dismiss()
//            self.navigationController!.dismissViewControllerAnimated(true, completion: nil)
        }
        // Sign In with credentials.
        let email = self.emailTextField.text
        let password = self.passwordTextField.text
        FIRAuth.auth()?.signInWithEmail(email!, password: password!) { (user, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            else {
                LogInManager.shared.isSignedIn = true;
                self.navigationController!.dismissViewControllerAnimated(true, completion: nil)
            }
            KRProgressHUD.dismiss()
        }
    }
    
    
    func dismissKeyboardOnTap() -> Void {
        
        view.endEditing(true);
    }

    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //        self.view.endEditing(true)
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {    //delegate method
        
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {  //delegate method
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField .resignFirstResponder();
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder();
        
        return true;
    }

    
}
