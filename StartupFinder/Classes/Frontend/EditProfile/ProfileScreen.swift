//
//  ProfileScreen.swift
//  StartupFinder
//
//  Created by Hassaan on 26/06/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import EMPageViewController

class ProfileScreen: UIViewController {
    
    internal func ProfileScreen() -> UIViewController {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let me = storyboard.instantiateViewControllerWithIdentifier("ProfileScreen")
        return me;
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: User Interaction
    
    @IBAction func btnSettingsPressed(sender: UIButton) {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Settings", bundle: nil);
        let settingsScreen = storyboard.instantiateInitialViewController()
        self.presentViewController(settingsScreen!, animated: true) {
            
        }
    }

    @IBAction func tourButtonPressed(sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Tour", bundle: nil);
        let tourBaseViewController = storyboard.instantiateInitialViewController() as! TourBaseViewController;
        let tourPage0 = storyboard.instantiateViewControllerWithIdentifier("TourPage0") as! TourPage0;
        let tourPage1 = storyboard.instantiateViewControllerWithIdentifier("TourPage1") as! TourPage1;
        let tourPage2 = storyboard.instantiateViewControllerWithIdentifier("TourPage2") as! TourPage2;
        let tourPage3 = storyboard.instantiateViewControllerWithIdentifier("TourPage3") as! TourPage3;
        
        tourBaseViewController.pageViewController = EMPageViewController(navigationOrientation: .Horizontal);
        tourBaseViewController.viewControllersArr = [tourPage0, tourPage1, tourPage2, tourPage3];
        
        self.presentViewController(tourBaseViewController, animated: true, completion: nil);
    }

}
