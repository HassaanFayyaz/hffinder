//
//  LinkAccountsScreen.swift
//  StartupFinder
//
//  Created by Hassaan on 03/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import Foundation
import KRProgressHUD
import MagicalRecord
import Alamofire
import Firebase
import FirebaseAuth

class LinkAccountsScreen: UIViewController  {

    override func viewDidLoad() {
        super.viewDidLoad()


        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        if let user = FIRAuth.auth()?.currentUser {
//            self.signedIn(user)
            print("There is a user in the Firebase Authentcation");
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    @IBAction func createAccountButtonPressed(sender: UIButton) {
        
        NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait();        
        
        KRProgressHUD.show(progressHUDStyle: .Black, message: "Finishing Setup...")
        
        let delay = dispatch_time(DISPATCH_TIME_NOW, Int64(2.0 * Double(NSEC_PER_SEC)))
        dispatch_after(delay, dispatch_get_main_queue()) {
//            KRProgressHUD.dismiss()
//            self.navigationController!.dismissViewControllerAnimated(true, completion: nil)
        }
        
        
        let currentUser : User = DataCoordinator.shared.getCurrentUser();

        let email = currentUser.email;
        let password = currentUser.password;
        FIRAuth.auth()?.createUserWithEmail(email!, password: password!) { (user, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                self.setDisplayName(user!)
            }
            KRProgressHUD.dismiss()
            self.navigationController!.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func setDisplayName(user: FIRUser) {
        let changeRequest = user.profileChangeRequest()
        changeRequest.displayName = user.email!.componentsSeparatedByString("@")[0]
        changeRequest.commitChangesWithCompletion(){ (error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
//            self.signedIn(FIRAuth.auth()?.currentUser)
        }
    }
    
//    func signedIn(user: FIRUser?) {
//        MeasurementHelper.sendLoginEvent()
//        
//        AppState.sharedInstance.displayName = user?.displayName ?? user?.email
//        AppState.sharedInstance.photoUrl = user?.photoURL
//        AppState.sharedInstance.signedIn = true
//        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NotificationKeys.SignedIn, object: nil, userInfo: nil)
//        performSegueWithIdentifier(Constants.Segues.SignInToFp, sender: nil)
//    }
}
