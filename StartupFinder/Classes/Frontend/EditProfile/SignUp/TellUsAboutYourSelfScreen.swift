//
//  TellUsAboutYourSelfScreen.swift
//  StartupFinder
//
//  Created by Hassaan on 03/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import Photos
import BSImagePicker

class TellUsAboutYourSelfScreen: UIViewController {

    @IBOutlet weak var btnUploadPhoto: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imageViewProfilePic: UIImageView!
    
    var selectedImageAsset : PHAsset = PHAsset();
    var selectedProfilePictures : NSMutableArray = NSMutableArray();
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imageViewProfilePic.layer.cornerRadius = self.imageViewProfilePic.bounds.size.height/2;
        self.imageViewProfilePic.clipsToBounds = true;
        
        let tapGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(TellUsAboutYourSelfScreen.profilePictureTapped));
        self.imageViewProfilePic.addGestureRecognizer(tapGestureRecognizer);
        

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        let profilePicId = NSUserDefaults.standardUserDefaults().stringForKey(Constants.profilePictureLocalId);
        self.profilePictureFromLocalId(profilePicId);
        
        if (profilePicId != nil) {
            let profilePics : UserImages = UserImages.MR_createEntity()!;
            profilePics.image1 = profilePicId;
            let currentUser : User = DataCoordinator.shared.getCurrentUser();
            currentUser.userImages = profilePics;
        }

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Upload Photo
    
    
    func profilePictureTapped()  {
        self.btnUploadPhotoPressed(nil);
    }

    @IBAction func btnUploadPhotoPressed(sender: UIButton?) {
        
        var retimage = UIImage();
        let manager = PHImageManager.defaultManager()
        let vc = BSImagePickerViewController()
        vc.settings.maxNumberOfSelections = 1;
        
        bs_presentImagePickerController(vc,
                                        animated: true,
                                        select:
            { (asset: PHAsset) -> Void in
                // User selected an asset.
                // Do something with it, start upload perhaps?
                manager.requestImageDataForAsset(asset, options: nil, resultHandler:
                    { (imageData, str, nil, result) in
                        // The Code may crash here.
                        retimage = UIImage(data: imageData!)!;
                        self.imageViewProfilePic.image = retimage;
                })
            }, deselect: { (asset: PHAsset) -> Void in
                // User deselected an assets.
                // Do something, cancel upload?
                self.selectedProfilePictures.removeObject(asset);
            }, cancel: { (assets: [PHAsset]) -> Void in
                // User cancelled. And this where the assets currently selected.
                self.selectedProfilePictures.removeAllObjects();
            }, finish: { (assets: [PHAsset]) -> Void in
                // User finished with these assets
                if assets.count > 0 {
                    self.selectedImageAsset = assets[0];
                    NSUserDefaults.standardUserDefaults().setObject(self.selectedImageAsset.localIdentifier, forKey: Constants.profilePictureLocalId )
                }
            }, completion: {
                
            })
        
    }
    
    func profilePictureFromLocalId(localId : NSString?) {
        if (localId == nil) {
            return;
        }
        
        let manager = PHImageManager.defaultManager()
        let allPhotosOptions = PHFetchOptions();
        let allPhotos = PHAsset.fetchAssetsWithOptions(allPhotosOptions);
        
        allPhotos.enumerateObjectsUsingBlock({ ( photoAsset, idx, stop) in
            
            if(localId == photoAsset.localIdentifier){
                
                manager.requestImageDataForAsset(photoAsset as! PHAsset, options: nil,
                    resultHandler: { (imageData, str, nil, result) in
                    // The Code may crash here.
                    self.imageViewProfilePic.image = UIImage(data: imageData!)!;
                })

        }
        })
    }

}
