//
//  PreviousExpertise.swift
//  StartupFinder
//
//  Created by Hassaan on 14/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import SwiftHEXColors

class PreviousExpertise: UIViewController , UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    var arr : NSArray = NSArray();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.arr = self.initializeDataSource() as! [NSArray];
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: Tableview Delegate and Datasource

//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return 1;
//    }
    
    // number of rows in table view
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr.count
    }
    
    // create a cell for each table view row
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell : PreviousExpertisCell = tableView.dequeueReusableCellWithIdentifier("PreviousExpertisCell") as! PreviousExpertisCell!
        
        // set the text from the data model
        let dict : NSDictionary = self.arr[indexPath.row] as! NSDictionary;
        cell.setCellDictionary(dict);
//        let colorCode : String = dict["title"] as! String;
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        tableview.deselectRowAtIndexPath(indexPath, animated: true);
    }
    
    // MARK: Lazy Instatiation
    

    func initializeDataSource() -> NSArray {
        return [["color" : UIColor.cyanColor(), "title" : "Engineering"],
        ["color" : UIColor.flatPumpkinColor() , "title" : "Medical"],
        ["color" : UIColor.magentaColor(), "title" : "Business"],
        ["color" : UIColor.redColor(), "title" : "Marketing"] ];
    }
}
