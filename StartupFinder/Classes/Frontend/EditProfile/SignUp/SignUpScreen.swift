//
//  SignUpScreen.swift
//  StartupFinder
//
//  Created by Hassaan on 02/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import IBAnimatable
import FirebaseAuth
import FirebaseDatabase
import KRProgressHUD

class SignUpScreen: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldFirstName: AnimatableTextField!
    @IBOutlet weak var textFieldMiddleName: AnimatableTextField!
    @IBOutlet weak var textFieldLastName: AnimatableTextField!
    @IBOutlet weak var textFieldUsername: AnimatableTextField!
    @IBOutlet weak var textFieldEmail: AnimatableTextField!
    @IBOutlet weak var textFieldPassword: AnimatableTextField!
    
    
    let currentUser : User = DataCoordinator.shared.getCurrentUser();

    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGestureRecognizer : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(SignUpScreen.dismissKeyboardOnTap));
        self.view.addGestureRecognizer(tapGestureRecognizer);
    
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        IQKeyboardManager.sharedManager().enable = true;
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated);
        
        IQKeyboardManager.sharedManager().enable = false;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: User Interaction

    @IBAction func btnExitPressed(sender: UIBarButtonItem) {
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil);
    }
    
    @IBAction func btnNextPressed(sender: AnyObject) {
        
        var tellUsAboutYourselfScreen: TellUsAboutYourSelfScreen?;
        tellUsAboutYourselfScreen = self.storyboard?.instantiateViewControllerWithIdentifier("TellUsAboutYourSelfScreen") as? TellUsAboutYourSelfScreen;
        
        self.navigationController?.pushViewController(tellUsAboutYourselfScreen!, animated: true);
    }
    
    func dismissKeyboardOnTap() -> Void {

        if self.textFieldFirstName.isFirstResponder() {
            self.textFieldFirstName.resignFirstResponder();
        }
        else if self.textFieldLastName.isFirstResponder() {
            self.textFieldLastName.resignFirstResponder();
        }
        else if self.textFieldMiddleName.isFirstResponder() {
            self.textFieldMiddleName.resignFirstResponder();
        }
        else if self.textFieldUsername.isFirstResponder() {
            self.textFieldUsername.resignFirstResponder();
        }
        else if self.textFieldPassword.isFirstResponder() {
            self.textFieldPassword.resignFirstResponder();
        }
        else if self.textFieldEmail.isFirstResponder() {
            self.textFieldEmail.resignFirstResponder();
        }
    }

    // MARK: SignUp User
    
    func validateInputData() -> Bool {
        
        var returnValue = true;
        if self.textFieldFirstName.text?.characters.count == 0 {
            self.textFieldFirstName.rightImage = UIImage.init(named: "0");
//            self.textFieldFirstName.shake();
            returnValue = false;
        }
        else if self.textFieldMiddleName.text?.characters.count == 0 {
            self.textFieldMiddleName.rightImage = UIImage.init(named: "0");
            self.textFieldMiddleName.shake();
            returnValue = false;
        }
        else if self.textFieldLastName.text?.characters.count == 0 {
            self.textFieldLastName.rightImage = UIImage.init(named: "0");
            self.textFieldLastName.shake();
            returnValue = false;
        }
        else if self.textFieldUsername.text?.characters.count == 0 {
            self.textFieldUsername.rightImage = UIImage.init(named: "0");
            self.textFieldUsername.shake();
            returnValue = false;
        }
        else if self.textFieldEmail.text?.characters.count == 0 {
            self.textFieldEmail.rightImage = UIImage.init(named: "0");
            self.textFieldEmail.shake();
            returnValue = false;
        }
        else if self.textFieldPassword.text?.characters.count == 0 {
            self.textFieldPassword.rightImage = UIImage.init(named: "0");
            self.textFieldPassword.shake();
            returnValue = false;
        }

        
        return returnValue;
    }
    
    func createAccount() {
        if self.validateInputData() {
             self.createAccountWith(self.textFieldFirstName.text!, middleName: self.textFieldMiddleName.text!, lastName: self.textFieldLastName.text!, userName: self.textFieldUsername.text!, email: self.textFieldEmail.text!, password: self.textFieldPassword.text!)
        }
        else {
            return ;
        }
        
    }
    
    func createAccountWith(firstName:String, middleName:String, lastName:String, userName:String, email:String, password:String)  {
     
        KRProgressHUD.show(progressHUDStyle: .Black, message: "Creating Account...");
        
        currentUser.firstName = firstName;
        currentUser.middleName = middleName;
        currentUser.lastName = lastName;
        currentUser.userName = userName;

        let rootRef = FIRDatabase.database().reference()
        var mdata = NSMutableDictionary();
        mdata = currentUser.toDictionary().mutableCopy() as! NSMutableDictionary;

        FIRAuth.auth()?.createUserWithEmail(email, password: password) { (user, error) in
            if let error = error {
                print("Could Not Create Account !");
                print(error.localizedDescription)
            } else {
                self.setDisplayName(user!)
                print("New Account Created !");
                var arr = rootRef.child("Users2");
                rootRef.child("Users2").childByAutoId().setValue(mdata)
                arr = rootRef.child("Users2");

                KRProgressHUD.showSuccess();
                
                dispatch_after(1*NSEC_PER_SEC, dispatch_get_main_queue(), {
                    self.btnNextPressed(NSNull);
                });
            }
        }

        
    }


    
    func setDisplayName(user: FIRUser) {
        let changeRequest = user.profileChangeRequest()
        changeRequest.displayName = user.email!.componentsSeparatedByString("@")[0]
        changeRequest.commitChangesWithCompletion(){ (error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            self.signedIn(FIRAuth.auth()?.currentUser)
        }
    }
    
    // MARK: // Managing user state
        func signedIn(user: FIRUser?) {
//            MeasurementHelper.sendLoginEvent()
    
            LogInManager.shared.displayName = user?.displayName ?? user?.email
            LogInManager.shared.photoUrl = user?.photoURL
            LogInManager.shared.isSignedIn = true
            NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notifications.IsSignIn, object: nil, userInfo: nil)
//            performSegueWithIdentifier(Constants.Segues.SignInToFp, sender: nil)
        }




    // MARK: UITextField Delegate

    func textFieldDidBeginEditing(textField: UITextField) {    //delegate method
        
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {  //delegate method
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField .resignFirstResponder();
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        if IQKeyboardManager.sharedManager().canGoNext {
            IQKeyboardManager.sharedManager().goNext();
            return false;
        }
        else {
            
            self.createAccount();
            textField.resignFirstResponder();
            return true;
        }
    }
}
