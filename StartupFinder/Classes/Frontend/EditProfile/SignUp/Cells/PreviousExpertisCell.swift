//
//  PreviousExpertisCell.swift
//  StartupFinder
//
//  Created by Hassaan on 15/07/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import IBAnimatable

class PreviousExpertisCell: UITableViewCell {
    
    
    var cellData : NSDictionary = NSDictionary();


    @IBOutlet weak var cellImageView: AnimatableImageView!
    
    @IBOutlet weak var cellTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        self.cellImageView.backgroundColor = self.cellData["color"] as? UIColor;
       super.setSelected(selected, animated: animated)

    }

    override func setHighlighted(highlighted: Bool, animated: Bool) {
        self.cellImageView.backgroundColor = self.cellData["color"] as? UIColor;
        super.setHighlighted(highlighted, animated: animated)
    }
    
    
    func setCellDictionary(dict : NSDictionary) {
        
        self.cellData = dict;
        self.cellTextLabel?.text = dict["title"] as? String;
        
//        let colorCode : String = dict["title"] as! String;
//        self.cellImageView.backgroundColor = UIColor(hexString: colorCode);
        self.cellImageView.backgroundColor = self.cellData["color"] as? UIColor;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64( 2*Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
            self.cellImageView.slideInLeft()
           });
        


        
    }

}
