//
//  TourBaseViewController.swift
//  StartupFinder
//
//  Created by Hassaan on 03/08/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import EMPageViewController
import IBAnimatable


class TourBaseViewController: UIViewController, EMPageViewControllerDataSource, EMPageViewControllerDelegate {

    
    @IBOutlet weak var pageControlDots: UIPageControl!
    @IBOutlet weak var signUpButton: AnimatableButton!
    
    var pageViewController: EMPageViewController?
    var viewControllersArr : NSArray?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        
        self.pageViewController!.dataSource = self
        self.pageViewController!.delegate = self

        // Set the initially selected view controller
        // IMPORTANT: If you are using a dataSource, make sure you set it BEFORE calling selectViewController:direction:animated:completion
        let currentViewController = self.viewControllerAtIndex(0)!
        self.pageViewController!.selectViewController(currentViewController, direction: .Forward, animated: false, completion: nil)
        
        // Add EMPageViewController to the root view controller
        self.addChildViewController(self.pageViewController!)
        self.view.insertSubview(self.pageViewController!.view!, atIndex: 0) // Insert the page controller view below the navigation buttons
        self.pageViewController?.didMoveToParentViewController(self)
        
//        self.pageViewController = pageViewController

        let swipeDownGesture : UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(TourBaseViewController.exitTour));
        swipeDownGesture.direction = .Down;
        self.view.addGestureRecognizer(swipeDownGesture);
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func exitTour() -> Void {
        self.dismissViewControllerAnimated(true, completion: nil);
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - Convienient EMPageViewController scroll / transition methods
    
    @IBAction func forward(sender: AnyObject) {
        self.pageViewController!.scrollForwardAnimated(true, completion: nil)
    }
    
    @IBAction func reverse(sender: AnyObject) {
        self.pageViewController!.scrollReverseAnimated(true, completion: nil)
    }
    
    @IBAction func scrollTo(sender: AnyObject) {
        
//        let choiceViewController = UIAlertController(title: "Scroll To...", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
//        
//        let selectedIndex = self.indexOfViewController(self.pageViewController!.selectedViewController as! GreetingViewController)
//        
//        for (index, viewControllerGreeting) in greetings.enumerate() {
//            
//            if (index != selectedIndex) {
//                
//                let action = UIAlertAction(title: viewControllerGreeting, style: UIAlertActionStyle.Default, handler: { (alertAction) in
//                    
//                    let viewController = self.viewControllerAtIndex(index)!
//                    
//                    let direction:EMPageViewControllerNavigationDirection = index > selectedIndex ? .Forward : .Reverse
//                    
//                    self.pageViewController!.selectViewController(viewController, direction: direction, animated: true, completion: nil)
//                    
//                })
//                
//                choiceViewController.addAction(action)
//            }
//        }
//        
//        let action = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
//        choiceViewController.addAction(action)
//        
//        self.presentViewController(choiceViewController, animated: true, completion: nil)
        
    }
    
    
    // MARK: - EMPageViewController Data Source
    
    func em_pageViewController(pageViewController: EMPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.indexOfViewController(viewController ) {
            let beforeViewController = self.viewControllerAtIndex(viewControllerIndex - 1)
            return beforeViewController
        } else {
            return nil
        }
    }
    
    func em_pageViewController(pageViewController: EMPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.indexOfViewController(viewController ) {
            let afterViewController = self.viewControllerAtIndex(viewControllerIndex + 1)
            return afterViewController
        } else {
            return nil
        }
    }
    
    func viewControllerAtIndex(index: Int) -> UIViewController? {
        if (index < 0) {
            return self.viewControllersArr![index + (self.viewControllersArr?.count)!] as? UIViewController;
        }
        else if (index >= self.viewControllersArr?.count) {
            return self.viewControllersArr![index % (self.viewControllersArr?.count)!] as? UIViewController;
        }
        return self.viewControllersArr![index] as? UIViewController;
    }
    
    func indexOfViewController(viewController: UIViewController) -> Int? {
//        if let greeting: String = viewController.greeting {
//            return self.greetings.indexOf(greeting)
//        } else {
//            return nil
//        }
        
        if viewController.isKindOfClass(TourPage0) {
            return 0;
        }
        else if viewController.isKindOfClass(TourPage1) {
            return 1;
        }
        else if viewController.isKindOfClass(TourPage2) {
            return 2;
        }
        else if viewController.isKindOfClass(TourPage3) {
            return 3;
        }
        return 0;
    }
    
    
    // MARK: - EMPageViewController Delegate
    
    func em_pageViewController(pageViewController: EMPageViewController, willStartScrollingFrom startViewController: UIViewController, destinationViewController: UIViewController) {
        
//        let startGreetingViewController = startViewController as! GreetingViewController
//        let destinationGreetingViewController = destinationViewController as! GreetingViewController
//        
//        print("Will start scrolling from \(startGreetingViewController.greeting) to \(destinationGreetingViewController.greeting).")
    }
    
    func em_pageViewController(pageViewController: EMPageViewController, isScrollingFrom startViewController: UIViewController, destinationViewController: UIViewController, progress: CGFloat) {
//        let startGreetingViewController = startViewController as! GreetingViewController
//        let destinationGreetingViewController = destinationViewController as! GreetingViewController
        
        // Ease the labels' alphas in and out
//        let absoluteProgress = fabs(progress)
//        startGreetingViewController.label.alpha = pow(1 - absoluteProgress, 2)
//        destinationGreetingViewController.label.alpha = pow(absoluteProgress, 2)
//        
//        print("Is scrolling from \(startGreetingViewController.greeting) to \(destinationGreetingViewController.greeting) with progress '\(progress)'.")
    }
    
    func em_pageViewController(pageViewController: EMPageViewController, didFinishScrollingFrom startViewController: UIViewController?, destinationViewController: UIViewController, transitionSuccessful: Bool) {
//        let startViewController = startViewController as! GreetingViewController?
//        let destinationViewController = destinationViewController as! GreetingViewController
        
        // If the transition is successful, the new selected view controller is the destination view controller.
        // If it wasn't successful, the selected view controller is the start view controller
        if transitionSuccessful {
            
            self.pageControlDots.currentPage = self.indexOfViewController(destinationViewController)!;
//            if (self.indexOfViewController(destinationViewController) == 0) {
//                self.reverseButton.enabled = false
//            } else {
//                self.reverseButton.enabled = true
//            }
//            
//            if (self.indexOfViewController(destinationViewController) == self.greetings.count - 1) {
//                self.forwardButton.enabled = false
//            } else {
//                self.forwardButton.enabled = true
//            }
        }
        
//        print("Finished scrolling from \(startViewController?.greeting) to \(destinationViewController.greeting). Transition successful? \(transitionSuccessful)")
    }
    
    
    //MARK: - User Interaction
    
    @IBAction func signUpButtonPressed(sender: AnimatableButton) {
    
        let storyboard = UIStoryboard(name: "SignUp", bundle: nil);
        let signUpScreen : SignUpScreen = storyboard.instantiateViewControllerWithIdentifier("SignUpScreen") as! SignUpScreen
        let navController :UINavigationController = UINavigationController(rootViewController: signUpScreen);
        self.presentViewController(navController, animated: true, completion: nil);

    }


}
