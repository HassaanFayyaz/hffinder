//
//  TourPage4.swift
//  StartupFinder
//
//  Created by Hassaan on 03/08/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit

class TourPage3: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self .addBackgroundGradientEffect();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: Blur + Gradient Effect
    
    func addBackgroundGradientEffect (){
        let gradientView = EZYGradientView();
        gradientView.frame = view.bounds;
        gradientView.firstColor = UIColor.clearColor();
        gradientView.secondColor = UIColor.clearColor();
        gradientView.angleº = 45.0;
        gradientView.colorRatio = 0.5;
        gradientView.fadeIntensity = 1;
        gradientView.isBlur = true;
        gradientView.blurOpacity = 1;
        
        //        self.view.insertSubview(gradientView, atIndex: 0);
        self.view.addSubview(gradientView);
        
    }


}
