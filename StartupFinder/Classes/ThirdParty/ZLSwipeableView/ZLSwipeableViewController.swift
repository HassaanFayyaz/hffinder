//
//  ViewController.swift
//  ZLSwipeableViewSwiftDemo
//
//  Created by Zhixuan Lai on 4/27/15.
//  Copyright (c) 2015 Zhixuan Lai. All rights reserved.
//

import UIKit
import UIColor_FlatColors
import Cartography
import ReactiveUI
import Firebase
import FirebaseDatabase

class ZLSwipeableViewController: UIViewController {

    var swipeableView: ZLSwipeableView!
    
    var colors = ["Silver", "Silver", "Concrete", "Silver", "Concrete",
                  "Silver",  "Silver", "Concrete"]
    var colorIndex = 0
    var loadCardsFromXib = true
    var totalUsersCount: Int?
    var usersDisplayed: Int = 0;
    var pulsingHalo: PulsingHaloLayer?
    
    var reloadBarButtonItem = UIBarButtonItem(title: "Reload", style: .Plain) { item in }
    var leftBarButtonItem = UIBarButtonItem(title: "←", style: .Plain) { item in }
    var upBarButtonItem = UIBarButtonItem(title: "↑", style: .Plain) { item in }
    var rightBarButtonItem = UIBarButtonItem(title: "→", style: .Plain) { item in }
    var downBarButtonItem = UIBarButtonItem(title: "↓", style: .Plain) { item in }
    
    var initialTranslation : CGPoint = CGPoint();
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        swipeableView.nextView = {
            return self.nextCardView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setToolbarHidden(false, animated: false)
        view.backgroundColor = UIColor.whiteColor()
        view.clipsToBounds = true
//        self.addBackgroundGradientEffect();

//        UIView* circle = [[UIView alloc] initWithFrame:CGRectMake(50, 50, 100, 100)];
//        [circle.layer setCornerRadius:circle.frame.size.width / 2];
//        [circle setBackgroundColor:[UIColor redColor]];
//        [self.view addSubview:circle];
//        
//        [UIView animateWithDuration:5 animations:^{
//            
//            // Animate it to double the size
//            const CGFloat scale = 2;
//            [circle setTransform:CGAffineTransformMakeScale(scale, scale)];
//            }];
//        let circle = PulseButton.init(frame: CGRectMake(50, 50, 50, 50));
//        circle.layer.cornerRadius = circle.frame.size.width/2;
//        circle.layer.borderWidth = 4.0;
//        circle.layer.borderColor = UIColor.blackColor().CGColor;
//        circle.backgroundColor = UIColor.redColor();
//        self.view.addSubview(circle);
//        circle.center = self.view.center;
        
        self.startPulsingHalo();
//        self.pulsingHalo?.performSelector(#selector(PulsingHaloLayer.stopAnimating), withObject: self.pulsingHalo, afterDelay: 10.0);
//        PulsingHaloLayer *layer = [PulsingHaloLayer layer];
//        self.halo = layer;
//        [self.beaconView.superview.layer insertSublayer:self.halo below:self.beaconView.layer];
//        
//        [self setupInitialValues];
//        
//        [self.halo start];

//
//        UIView.animateWithDuration(2) { 
//            let scale: CGFloat = 2.0;
//            circle.transform = CGAffineTransformMakeScale(scale, scale);
//            circle.alpha = 0;
//        }
//        circle.startAnimating();
        reloadBarButtonItem.addAction() { item in
            let alertController = UIAlertController(title: nil, message: "Load Cards:", preferredStyle: .ActionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                // ...
            }
            alertController.addAction(cancelAction)
            
            let ProgrammaticallyAction = UIAlertAction(title: "Programmatically", style: .Default) { (action) in
                self.loadCardsFromXib = false
                self.colorIndex = 0
                self.swipeableView.discardViews()
                self.swipeableView.loadViews()
            }
            alertController.addAction(ProgrammaticallyAction)
            
            let XibAction = UIAlertAction(title: "From Xib", style: .Default) { (action) in
                self.loadCardsFromXib = true
                self.colorIndex = 0
                self.swipeableView.discardViews()
                self.swipeableView.loadViews()
            }
            alertController.addAction(XibAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        leftBarButtonItem.addAction() { item in
            self.swipeableView.swipeTopView(inDirection: .Left)
        }
        upBarButtonItem.addAction() { item in
            self.swipeableView.swipeTopView(inDirection: .Up)
        }
        rightBarButtonItem.addAction() { item in
            self.swipeableView.swipeTopView(inDirection: .Right)
        }
        downBarButtonItem.addAction() { item in
            self.swipeableView.swipeTopView(inDirection: .Down)
        }
        
        let fixedSpace = UIBarButtonItem(barButtonSystemItem: .FixedSpace, action: {item in})
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, action: {item in})
        
        let items = [fixedSpace, reloadBarButtonItem, flexibleSpace, leftBarButtonItem, flexibleSpace, upBarButtonItem, flexibleSpace, rightBarButtonItem, flexibleSpace, downBarButtonItem, fixedSpace]
        toolbarItems = items

        swipeableView = ZLSwipeableView()
        view.addSubview(swipeableView)
        swipeableView.didStart = {view, location, translation in
            print("Did start swiping view at location: \(location) translation: \(translation)")
            self.initialTranslation = translation;
        }
        swipeableView.swiping = {view, location, translation in
            print("Swiping at view location: \(location) translation: \(translation)")
            let backgroundColorView = view.subviews[0].viewWithTag(Constants.ZLSwipeableViewBackgroundColorViewTag);
            
            
            if let label = view.subviews[0].viewWithTag(Constants.ZLSwipeableViewLikeDislikeLabelTag) as? UILabel {
                label.hidden = false
                if translation.x < 0 {
                    backgroundColorView?.backgroundColor = UIColor(red: 0.9, green: 0, blue: 0, alpha: 0.8);
                    label.text = "Dislike";
                    label.alpha = translation.x * -0.005;
                    backgroundColorView?.alpha = label.alpha;
                }
                else {
                    backgroundColorView?.backgroundColor = UIColor(red: 0, green: 0.85, blue: 0, alpha: 0.8);
                    label.text = "Like";
                    label.alpha = translation.x * 0.005;
                    backgroundColorView?.alpha = label.alpha;
                }
            }

        }
        swipeableView.didEnd = {view, location in
            print("Did end swiping view at location: \(location)")
        }
        swipeableView.didSwipe = {view, direction, vector in
            print("Did swipe view in direction: \(direction), vector: \(vector)")
            if direction == .Right {
                print("RIGHT SWIPE !!! ")
                view.subviews[0].backgroundColor = UIColor(red: 0, green: 0.85, blue: 0, alpha: 0.8);
                
                if let label = view.subviews[0].viewWithTag(Constants.ZLSwipeableViewLikeDislikeLabelTag) as? UILabel {
                    label.text = "Like"
                    label.alpha = 1.0;
                }
                
            }
            else if direction == .Left {
                print("LEFT SWIPE !!! ")
                view.subviews[0].backgroundColor = UIColor(red: 0.9, green: 0, blue: 0, alpha: 0.8);

                if let label = view.subviews[0].viewWithTag(Constants.ZLSwipeableViewLikeDislikeLabelTag) as? UILabel {
                    label.text = "Dislike"
                    label.alpha = 1.0;
                }
                
            }
        }
        swipeableView.didCancel = {view in
            print("Did cancel swiping view")
            if let label = view.subviews[0].viewWithTag(Constants.ZLSwipeableViewLikeDislikeLabelTag) as? UILabel {
                label.alpha = 0.0;
            }
            view.subviews[0].viewWithTag(Constants.ZLSwipeableViewBackgroundColorViewTag)?.alpha = 0.0;

            
        }
        swipeableView.didTap = {view, location in
            print("Did tap at location \(location)")
        }
        swipeableView.didDisappear = { view in
            print("Did disappear swiping view")
            
            print("Active Views : \(self.swipeableView.activeViews().count)")
            if self.swipeableView.activeViews().count == 0 {
                self.startPulsingHalo();
            }
        }

        constrain(swipeableView, view) { view1, view2 in
            view1.left == view2.left+40
            view1.right == view2.right-40
            view1.top == view2.top + 100
            view1.bottom == view2.bottom - 80
        }
        
        self.totalUsersCount = {
            return FirebaseManager.shared.users.count;
        }()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        self.totalUsersCount = FirebaseManager.shared.users.count;
        print("Total Users : \(self.totalUsersCount)");
    }
    
    // MARK: ()
    func nextCardView() -> UIView? {
        if self.usersDisplayed >= FirebaseManager.shared.users.count {
            return nil;
        }
        self.stopPulsingHalo()
        if colorIndex >= colors.count {
            colorIndex = 0
        }

        let cardView = CardView(frame: swipeableView.bounds)
        cardView.backgroundColor = colorForName(colors[colorIndex])
        colorIndex += 1

        if loadCardsFromXib {
            let contentView = NSBundle.mainBundle().loadNibNamed("CardContentView", owner: self, options: nil).first! as! UIView
            contentView.translatesAutoresizingMaskIntoConstraints = false
            contentView.backgroundColor = cardView.backgroundColor
            cardView.addSubview(contentView)

            // This is important:
            // https://github.com/zhxnlai/ZLSwipeableView/issues/9
            /*// Alternative:
            let metrics = ["width":cardView.bounds.width, "height": cardView.bounds.height]
            let views = ["contentView": contentView, "cardView": cardView]
            cardView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView(width)]", options: .AlignAllLeft, metrics: metrics, views: views))
            cardView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView(height)]", options: .AlignAllLeft, metrics: metrics, views: views))
            */
            constrain(contentView, cardView) { view1, view2 in
                view1.left == view2.left
                view1.top == view2.top
                view1.width == cardView.bounds.width
                view1.height == cardView.bounds.height
            }
        }
        
        let userSnapshot: FIRDataSnapshot! = FirebaseManager.shared.users[usersDisplayed];
        print("\(userSnapshot.value!["userName"])")
        let name = (userSnapshot.value!["userName"]);
        let nameLabel : UILabel = cardView.viewWithTag(87) as! UILabel;
        nameLabel.text = name as? String;
//        cardView.lblName.text = "Hassaan";
//        cardView.lblExpertise.text = (userSnapshot.value!["userName"]);
//        cardView.lblName.text = FirebaseManager.shared.users[usersDisplayed].value!["firstName"];
        usersDisplayed += 1;
        return cardView
    }

    func colorForName(name: String) -> UIColor {
        let sanitizedName = name.stringByReplacingOccurrencesOfString(" ", withString: "")
        let selector = "flat\(sanitizedName)Color"
        return UIColor.performSelector(Selector(selector)).takeUnretainedValue() as! UIColor
    }
    
    // MARK: Blur Effect
    
    func addBackgroundGradientEffect (){
        let gradientView = EZYGradientView();
        gradientView.frame = view.bounds;
        gradientView.firstColor = UIColor(red: 0.84, green: 0.3, blue: 0.2, alpha: 1.0);
        gradientView.secondColor = UIColor(red: 0.64, green: 0.4, blue: 0.4, alpha: 1.0);
        gradientView.angleº = 45.0;
        gradientView.colorRatio = 0.5;
        gradientView.fadeIntensity = 1;
        gradientView.isBlur = true;
        gradientView.blurOpacity = 0.5;
        
        self.view.insertSubview(gradientView, atIndex: 0);
    }
    
    // MARK: Pulsing Halo
    
    func startPulsingHalo() {

        self.pulsingHalo = nil;
        self.pulsingHalo = PulsingHaloLayer();
        self.pulsingHalo?.position = self.view.center;
        self.view.layer.insertSublayer(self.pulsingHalo!, atIndex: 0);
        self.pulsingHalo?.haloLayerNumber = 2;
        self.pulsingHalo?.radius = UIScreen.mainScreen().bounds.size.width/2;
        self.pulsingHalo?.animationDuration = 4;
        
        self.pulsingHalo?.startAnimating();
    }
    
    func stopPulsingHalo() {
        self.pulsingHalo?.stopAnimating();
        self.pulsingHalo = nil;
    }
}

