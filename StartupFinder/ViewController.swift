//
//  ViewController.swift
//  StartupFinder
//
//  Created by Hassaan on 23/06/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import Alamofire
import EMPageViewController
import Firebase
import FirebaseAuth


class ViewController: UIViewController {
    

    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnSignOut: UIButton!

    private var _refHandle: FIRDatabaseHandle!
    var ref: FIRDatabaseReference!
    var messages: [FIRDataSnapshot]! = []
    var msglength: NSNumber = 100

    
    internal func CentreScreen() -> UIViewController {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let me = storyboard.instantiateInitialViewController();
        return me!;
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        

//        var uid = user?.uid;
        ref = FIRDatabase.database().reference()
//        if let user = FIRAuth.auth()?.currentUser {
//            self.ref.child("Users2/\(user.uid)/userName").setValue("Some Updated Name");
//        }

        // Listen for new messages in the Firebase database
//        _refHandle = self.ref.child("Users2").observeEventType(.ChildAdded, withBlock: { (snapshot) -> Void in
//            let text = snapshot.value!["userName"] as? String
//            let sender = snapshot.value!["firstName"] as? String
//            print("First Name : \(text) \nUserName : \(sender)");
//            self.messages.append(snapshot)
//            
//            let alert : UIAlertController = UIAlertController(title: "New User Added", message: "User Added to the database. Check if he suits you.", preferredStyle: .Alert);
//            self.presentViewController(alert, animated: true, completion: nil);
//            dispatch_after(NSEC_PER_SEC*3, dispatch_get_main_queue(), {
//                alert.dismissViewControllerAnimated(true, completion: nil);
//            })
//        })

        //        let rootRef = FIRDatabase.database().reference()
//        rootRef.setValue("Hello World !");
        
//        let mdata = NSMutableDictionary();
//        mdata["userName"] = "Hassaan";
//        mdata["password"] = "password";
//        // Push data to Firebase Database
//        var arr = rootRef.child("Users");
//        rootRef.child("Users").childByAutoId().setValue(mdata)
//        arr = rootRef.child("Users");
        
        FirebaseManager.shared.retrieveUsers();
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        if let user = FIRAuth.auth()?.currentUser {
//            self.signedIn(user)
            LogInManager.shared.isSignedIn = true;
            self.btnSignIn.enabled = false;
            self.btnSignOut.enabled = true;
        }
        else {
            self.btnSignOut.enabled = false;
            self.btnSignIn.enabled = true;
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func SignUpButtonPressed(sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "SignUp", bundle: nil);
        let signUpScreen : SignUpScreen = storyboard.instantiateViewControllerWithIdentifier("SignUpScreen") as! SignUpScreen
        let navController :UINavigationController = UINavigationController(rootViewController: signUpScreen);
        self.presentViewController(navController, animated: true, completion: nil);
        
    }
    @IBAction func signInButtonPressed(sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "SignIn", bundle: nil);
        let signUpScreen : SignInScreen = storyboard.instantiateViewControllerWithIdentifier("SignInScreen") as! SignInScreen
        let navController :UINavigationController = UINavigationController(rootViewController: signUpScreen);
        self.presentViewController(navController, animated: true, completion: nil);
    }
    
    @IBAction func signOutButtonPressed(sender: UIButton) {
        let firebaseAuth = FIRAuth.auth()
        do {
            try firebaseAuth?.signOut()
            LogInManager.shared.isSignedIn = false
//            dismissViewControllerAnimated(true, completion: nil)
            self.btnSignIn.enabled = true;
            self.btnSignOut.enabled = false;
        } catch let signOutError as NSError {
            print ("Error signing out: \(signOutError)")
            self.btnSignOut.enabled = false;
            self.btnSignIn.enabled = true;
        }
        
    }
}

