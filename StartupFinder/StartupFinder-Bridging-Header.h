//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//


#import "Constant.h"

// Tinnderlike Pageview
#import "TETinderPageView.h"
#import "PulsingHalo.h"

// Magical Records
//#import "CoreData+MagicalRecord.h"
//#import <MagicalRecord/MagicalRecord.h>
//#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>
//#import <MagicalRecord/MagicalRecordShorthandMethodAliases.h>

