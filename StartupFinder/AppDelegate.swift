
//
//  AppDelegate.swift
//  StartupFinder
//
//  Created by Hassaan on 23/06/2016.
//  Copyright © 2016 Hassaan. All rights reserved.
//

import UIKit
import MagicalRecord
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var pageView: TETinderPageView?
    var page1: ProfileScreen?
    var page2: ZLSwipeableViewController?
    var page3: ViewController?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        FIRApp.configure();
        FIRDatabase.database().persistenceEnabled = true
        
        self.loaddefaultValues();
        
        self.setupCoreData(); // Move this to the dispatch_once of DataCoordinator
        
        DataCoordinator.shared; // initialize DataCoordinator
        
//        let currentUser : User = DataCoordinator.shared.getCurrentUser();
//        currentUser.userName = "Moeez";
 
        self.loadUI();
        
        
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window!.rootViewController = pageView
        self.window!.makeKeyAndVisible()
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func loadUI() {
        
        var profileScreen : ProfileScreen = ProfileScreen()
        profileScreen = profileScreen.ProfileScreen() as! ProfileScreen
        self.page1 = profileScreen
        
        self.page2 = ZLSwipeableViewController();

        self.page3 = ViewController();
        self.page3 = self.page3!.CentreScreen() as? ViewController;
        self.page3!.view!.backgroundColor = UIColor.orangeColor();
        
        let viewControllers: [AnyObject] = [page1!, page2!, page3!]
        let images: [AnyObject] = [UIImage(named: "0.png")!, UIImage(named: "1.png")!, UIImage(named: "2.png")!]
        pageView = TETinderPageView(viewControllers: viewControllers, buttonImages: images)
        
        
    }
    
    
    func loaddefaultValues()  {
        
        NSUserDefaults.standardUserDefaults().registerDefaults(
            [kMaximumTeamSize : Int(kMaximumTeamSizeDefault),
                kStartupType : kStartupTypeDefault]);
        
    }

    
    func setupCoreData() {
        
        MagicalRecord.enableShorthandMethods();
        MagicalRecord.setupCoreDataStackWithStoreNamed("StartupFincderModel");
        
    }

}

